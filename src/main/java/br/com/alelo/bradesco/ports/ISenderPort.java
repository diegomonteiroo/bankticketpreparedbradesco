package br.com.alelo.bradesco.ports;

public interface ISenderPort {
	void sender(String message);
}