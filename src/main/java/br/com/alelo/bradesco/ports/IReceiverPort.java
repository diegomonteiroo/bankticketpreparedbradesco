package br.com.alelo.bradesco.ports;

public interface IReceiverPort {
	void receive(String message);
}