package br.com.alelo.bradesco.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;

import br.com.alelo.bradesco.ports.IReceiverPort;

public class ReceiverAdapterRegisterBradPrepared implements IReceiverPort{
	@Autowired
	private JmsTemplate jmsTemplate;
		
	@Value("${application.fila.jms.bnk_register_brad_prepared}")
	private String filaBnkRegisterBradPrepared; 
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverAdapterRegisterBradPrepared.class);

	@Override
	public void receive(String message) {
		LOGGER.info("Mensagem recebida \n" + message);
		jmsTemplate.convertAndSend(filaBnkRegisterBradPrepared, message);
	}
}