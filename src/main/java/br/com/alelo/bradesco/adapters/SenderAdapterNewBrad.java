package br.com.alelo.bradesco.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import br.com.alelo.bradesco.ports.ISenderPort;

public class SenderAdapterNewBrad implements ISenderPort{
	
	@Autowired
	private ReceiverAdapterRegisterBradPrepared receiverPrepared;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SenderAdapterNewBrad.class);
	
	@Override
	@JmsListener(destination = "${application.fila.jms.bnk_register_brad_new}")
	public void sender(String message) {
		LOGGER.info("Mensagem recebida da Fila bnk.register.brad.new");
		LOGGER.info("Passando a mensagem para a fila bnk.register.brad.prepared " + message);
		receiverPrepared.receive(message);
	}
}