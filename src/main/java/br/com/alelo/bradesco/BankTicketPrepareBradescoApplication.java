package br.com.alelo.bradesco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankTicketPrepareBradescoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankTicketPrepareBradescoApplication.class, args);
	}
}