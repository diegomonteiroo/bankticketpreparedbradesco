package br.com.alelo.bradesco.core.domain;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

public class PreparedBradesco implements Serializable {

	private static final long serialVersionUID = -7631304275067939728L;
	
	@ApiModelProperty(dataType = "String", notes = "Campo payment-assignor-document-number não deve ser nulo", required = true)
	@SerializedName(value = "payment-assignor-document-number")
	private String assignorDocumentNumber;
	
	@ApiModelProperty(dataType = "String", notes = "Campo payment-assignor-document-type não deve ser nulo", required = true)		
	@SerializedName(value = "payment-assignor-document-type")
	private String assignorDocumentType;

	@ApiModelProperty(dataType = "String", notes = "Campo payment-provider-portfolio não deve ser nulo", required = true)	
	@SerializedName(value = "payment-provider-portfolio")
	private String providerPortfolio;

	@ApiModelProperty(dataType = "String", notes = "Campo payment-emission-date não deve ser nulo", required = true)
	@SerializedName(value = "payment-emission-date")
	private String emissionDate;

	@ApiModelProperty(dataType = "String", notes = "Campo payment-expirateion-date não deve ser nulo", required = true)
	@SerializedName(value = "payment-expirateion-date")
	private String expirationDate;

	@ApiModelProperty(dataType = "String", notes = "Campo payment-amount não deve ser nulo", required = true)
	@SerializedName(value = "payment-amount")
	private String amout; 

	@ApiModelProperty(dataType = "String", notes = "payment-type não deve ser nulo", required = true)
	@SerializedName(value = "payment-type")
	private String type;
	
	
}