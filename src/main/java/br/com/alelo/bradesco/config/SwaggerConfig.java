package br.com.alelo.bradesco.config;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	private static final String BASE_PACKAGE = "br.com.alelo.bradesco.core.domain";
	private static final String TITLE = "API PREPARED BRADESCO";
	private static final String DESCRIPTION = "Api responsavel por realizar o envio de um boleto ao banco do brasil";
	private static final String VERSION = "1.0.0";
	private static final String LICENSE = "Copyright  - Alelo 2019";
	
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
				.paths(PathSelectors.any())
				.build().apiInfo(metaData());
	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder()
				.title(TITLE)
				.description(DESCRIPTION)
				.version(VERSION)
				.license(LICENSE)
				.build();
	}
}